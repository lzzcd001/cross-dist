import os
# `pip install easydict` if you don't have it
from easydict import EasyDict as edict

cache_type = 'pkl'

data_dir_dict = edict()

######

# The original dataset and the target dataset
data_dir_dict.scene_original_dataset_dir = '/home/tom/rgbd-scenes-v2/imgs'
data_dir_dict.object_original_dataset_dir = '/home/tom/rgbd-dataset'
data_dir_dict.final_dataset_dir = '/home/tom/object_slam_datasets/rcnn_hha_dataset_loo'
data_dir_dict.edgeboxes_dataset_dir = os.path.join(data_dir_dict.final_dataset_dir, 'edgeboxes')
data_dir_dict.ground_truth_cache_dir = os.path.join(data_dir_dict.final_dataset_dir, 'ground_truth_cache_' + cache_type)
data_dir_dict.sam_dir = os.path.join(data_dir_dict.final_dataset_dir, 'sam_gt')
######

# Object label file
object_list_filepath = '/home/tom/rgbd-dataset/uwLabels.txt'

# Select the objects that we care about
# CAUTION: ALWAYS place __background__ as the first item !!!

class_sel_list =  ('__background__', # always index 0
            'bowl', 'cap', 'cereal_box', 'coffee_mug', 'food_box', 'food_can',
            'food_cup', 'food_jar', 'soda_can')

# All UW object dataset images are resized to the following size
# Should be consistent with your data preprocessing
# Default: (256, 256)
object_dataset_resize_shape = (256, 256)
