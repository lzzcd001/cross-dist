import os
import cv2
import json
import pprint
import cPickle
import shutil
import numpy as np
from utils.data_utils import *
from uw_config import cache_type, object_list_filepath, data_dir_dict, class_sel_list, object_dataset_resize_shape

def cache_ground_truth_from_uw_object_dataset(uw_dataset_root, dataset_dir, class_list_filename, cache_type='pkl', resize_shape=(256,256)):
	'''
	Assuming all image files are in PNG.

	Assuming that the target dataset directory is organized as the following structure:
		dataset_dir
		-- rgb
		---- <all rgb files>
		-- depth
		---- <all depth files>
		-- hha
		---- <all hha files>
		...
		<continue>

	All image files (rgb, depth, hha, ...) should follow the same format.
	For example, for instance "apple_0_0_0", both rgb and depth filename should be "apple_0_0_0.png"

	Class id starts from 1 because 0 is reserved for background

	'''

	# make ground truth cache directory
	cache_dir = os.path.join(dataset_dir, 'ground_truth_cache_' + cache_type)
	exist_or_mkdir(cache_dir)

	# Get the map from object class name to a specific class number (in alphebatical order)
	class_name_to_order = {}
	class_order_to_name = {}
	with open(class_list_filename, 'r') as class_raw_list:
		class_list = class_raw_list.readlines()
		for class_combo in enumerate(class_list):
			class_name_to_order[class_combo[1].rstrip()] = class_combo[0]
			class_order_to_name[class_combo[0]] = class_combo[1].rstrip()

	# use rgb files to determine what instances of objects we have
	class_folder_list = get_immediate_subdirectories(uw_dataset_root)

	for class_folder in class_folder_list:
		exist_or_mkdir(os.path.join(cache_dir, class_folder))
		class_dir = os.path.join(uw_dataset_root, class_folder)

		image_filename_list = get_immediate_files(class_dir)
		for image_filename in image_filename_list:
			if image_filename[-9:] != '_crop.png':
				continue
			image_file = cv2.imread(os.path.join(class_dir, image_filename))

			# Get bounding box of objects. Starting from 1 (instead of 0)
			left_x = 1
			left_y = 1
			right_x, right_y = resize_shape

			#### Assuming the file extension is PNG
			filename_stem = class_folder + '/' + image_filename[:-9]

			# The instance id is set to one because we only do class level detection now. 
			# Can be changed.
			obj_name = class_folder
			objects = [
						{'class': obj_name,
						'bbox': [left_x, left_y, right_x, right_y],
						'instanceId': 1,
						'difficult': 0,
						'truncated': 0
						}
					]
			ground_truth_info = {'objects': objects,
							'type': 'category',
							'imgsize': [right_x, right_y],
							'fname': filename_stem}

			if cache_type == 'json':
				cache_filepath = os.path.join(cache_dir, filename_stem + '.json')
				with open(cache_filepath, 'w') as cache_file:
					json.dump(ground_truth_info, cache_file)
			elif cache_type == 'pkl':
				cache_filepath = os.path.join(cache_dir, filename_stem + '.pkl')
				with open(cache_filepath, 'w') as cache_file:
					cPickle.dump(ground_truth_info, cache_file)
			elif cache_type == 'mat':
				raise NotImplementedError
			else:
				raise NotImplementedError


def cache_ground_truth_from_uw_scene(scene_dataset_dir, dataset_dir, class_list_filename,
							class_sel_list=None, cache_type='pkl', interpolation=True):

	# make ground truth cache directory
	cache_dir = os.path.join(dataset_dir, 'ground_truth_cache_' + cache_type)
	exist_or_mkdir(cache_dir)

	# Get the map from object class name to a specific class number (in alphebatical order)
	class_name_to_order = {}
	class_order_to_name = {}
	with open(class_list_filename, 'r') as class_raw_list:
		class_list = class_raw_list.readlines()
		for class_combo in enumerate(class_list):
			class_name_to_order[class_combo[1].rstrip()] = class_combo[0]
			class_order_to_name[class_combo[0]] = class_combo[1].rstrip()

	scece_list = [scene_name for scene_name in get_immediate_subdirectories(scene_dataset_dir)
					if scene_name[-3:] != 'rgb']

	for scece_id, scene in enumerate(scece_list):
		cache_scene_dir = os.path.join(cache_dir, scene)
		exist_or_mkdir(cache_scene_dir)

		# Get what kinds of objects appear in this scene
		annotation_filepath = os.path.join(scene_dataset_dir, scene, 'annotation/index.json') # Annotation information file
		annotation_file = open(annotation_filepath, 'r')
		data = json.load(annotation_file)
		annotation_file.close()


		total_instance_list = data['objects']
		frameCounter = set() # To deternune whether a frame is already used
		frameWithAnnotation_list = []

		for frame_id in xrange(len(data['frames'])):
			frame_instance_list = data['frames'][frame_id].keys()
			filtered_list = [instance for instance in frame_instance_list if instance is not None]
			if len(filtered_list) != 0:
				filename_stem = scene + '/' + ('%.5d' % frame_id)
				object_info_list = []
				for j in xrange(len(data['frames'][frame_id]['polygon'])):
					obj_index = data['frames'][frame_id]['polygon'][j]['object']
					obj_name_and_instanceId = total_instance_list[obj_index]['name']
					if ': ' in obj_name_and_instanceId:
						obj_name, instanceId = obj_name_and_instanceId.split(': ')
					else:
						obj_name = obj_name_and_instanceId

					# Get bounding box of objects
					left_x, right_x = min_and_max(data['frames'][frame_id]['polygon'][j]['x'])
					left_y, right_y = min_and_max(data['frames'][frame_id]['polygon'][j]['y'])

					if right_x <= left_x or right_y <= left_y:
						continue

					if class_sel_list != None:
						if obj_name not in class_sel_list:
							# obj_name = '__background__'
							continue
					# The instance id is set to one because we only do class level detection now. 
					# Can be changed.
					object_info = {'class': obj_name, 
								'bbox': [left_x, left_y, right_x, right_y],
								'instanceId': 1,
								'difficult': 0,
								'truncated': 0
								}
					object_info_list.append(object_info)
				if len(object_info_list) == 0:
					continue
				ground_truth_info = {'objects': object_info_list,
								'type': 'scene',
								'imgsize': [640, 480],
								'fname': filename_stem}

				frameCounter.add(frame_id)
				frameWithAnnotation_list.append(frame_id)
				# Note: do NOT use cache_scene_dir because filename_stem already takes care of that
				if cache_type == 'json':
					cache_filepath = os.path.join(cache_dir, filename_stem + '.json')
					with open(cache_filepath, 'w') as cache_file:
						json.dump(ground_truth_info, cache_file)
				elif cache_type == 'pkl':
					cache_filepath = os.path.join(cache_dir, filename_stem + '.pkl')
					with open(cache_filepath, 'w') as cache_file:
						cPickle.dump(ground_truth_info, cache_file)
				elif cache_type == 'mat':
					raise NotImplementedError
				else:
					raise NotImplementedError

		if interpolation:
			for base_frame_id in frameWithAnnotation_list:
				frame_range = range(
					np.max([base_frame_id - 10, 0]), 
					np.min([base_frame_id+11, len(data['frames'])])
					)
				for frame_id in frame_range:
					if frame_id in frameCounter:
						continue
					else:
						frameCounter.add(frame_id)
					frame_instance_list = data['frames'][frame_id].keys()
					filename_stem = scene + '/' + ('%.5d' % frame_id)
					base_filename_stem = scene + '/' + ('%.5d' % base_frame_id)

					# Note: do NOT use cache_scene_dir because filename_stem already takes care of that
					if cache_type == 'json':
						cache_filepath = os.path.join(cache_dir, filename_stem + '.json')
						base_cache_filepath = os.path.join(cache_dir, base_filename_stem + '.json')
					elif cache_type == 'pkl':
						cache_filepath = os.path.join(cache_dir, filename_stem + '.pkl')
						base_cache_filepath = os.path.join(cache_dir, base_filename_stem + '.pkl')
					elif cache_type == 'mat':
						raise NotImplementedError
					else:
						raise NotImplementedError

					shutil.copy(base_cache_filepath, cache_filepath)


def cache(class_sel_list):
	# ===================================================
	# dirs
	class_list_filename = object_list_filepath
	dataset_dir = data_dir_dict.final_dataset_dir
	uw_object_root_path = data_dir_dict.object_original_dataset_dir
	scene_root_path = data_dir_dict.scene_original_dataset_dir
	# ===================================================
	print 'starting ground truth cache...'
	cache_ground_truth_from_uw_object_dataset(uw_object_root_path, dataset_dir, class_list_filename, cache_type=cache_type, resize_shape=object_dataset_resize_shape)
	cache_ground_truth_from_uw_scene(scene_root_path, dataset_dir, class_list_filename, class_sel_list=class_sel_list, cache_type=cache_type)
	print 'finished ground truth cache'

if __name__ == "__main__":
	cache(class_sel_list)
	