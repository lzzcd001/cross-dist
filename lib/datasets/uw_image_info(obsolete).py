import os
import re
import cv2
import json
import pprint
import cPickle
import scipy.misc
import numpy as np
from collections import Counter

def ground_truth_UW(object_raw_list, dataset_filename, object_list_file):

	# Regex to parse the label name
	r = re.compile('[a-z_]+/[a-z_]+_[0-9]+_[0-9]+')

	# Get the map from object class name to a specific number (in alphebatical order)
	object_name_to_order = {}
	with open(object_list_file, 'r') as object_raw_list:
		object_list = object_raw_list.readlines()
		for obj_combo in enumerate(object_list):
			object_name_to_order[obj_combo[1].rstrip()] = obj_combo[0]


	gt_data_list = []
	with open(dataset_filename, 'r') as dataset_file:
		content = map(lambda x: x.rstrip().split(' '), dataset_file.readlines())
		content = content[::20]
		# random_inds = np.random.choice(len(content), int(len(content) * 0.1), replace=False)
		# content = content[random_inds]
		filename_list = map(lambda x: x[0], content)
		class_list = map(lambda x: x[1], content)
		for ind, filename in enumerate(filename_list):
			tmp_img = cv2.imread(filename)
			right_x, right_y = tmp_img.shape[:2]

			# Get bounding box of objects
			left_x = 1
			left_y = 1

			try:
				filename_start_ind = re.search(r, filename).start()
			except:
				print
				print filename
				print
				raise
			#### Assuming the file extension is PNG
			filename_stem = filename[filename_start_ind: -4]

			substruct = {'class': str(int(class_list[ind]) + 1), 
						'bbox': [left_x, left_y, right_x, right_y],
						'instanceId': 1,
						'difficult': 0,
						'truncated': 0
						}
			objects = [substruct]
			# objects.append(substruct)
			current_struct = {'objects': objects,
							'type': 'category',
							'imgsize': [right_x, right_y],
							'frame_no': '%.5d' % ind,
							'fname': filename_stem}

			gt_data_list.append(current_struct)
	return gt_data_list, ind

def ground_truth_oneScene(scene_root_path, scene_paths, object_list_file, count_init = 0):
	count_init -= 1

	# Regex to parse the label name
	r = re.compile('[a-z_]+')

	# get this scene folder
	scenes = [os.path.join(scene_root_path, one_scene_path) for one_scene_path in scene_paths]

	# Get the map from object class name to a specific number (in alphebatical order)
	object_name_to_order = {}
	with open(object_list_file, 'r') as object_raw_list:
		object_list = object_raw_list.readlines()
		for obj_combo in enumerate(object_list):
			object_name_to_order[obj_combo[1].rstrip()] = obj_combo[0]

	gt_data_list = []
	output_image_number = 0
	total_count = count_init
	for kk, scene in enumerate(scenes):
		json_path = os.path.join(scene, 'annotation/index.json') # Annotation information file
		with open(json_path, 'r') as annotation_file:
			frameCounter = Counter() # To deternune whether a frame is already used
			# Get what kinds of objects appear in this scene
			data = json.load(annotation_file)
			existing_objects = [ r.search(data['objects'][i]['name']).group() for i in xrange(len(data['objects'])) if data['objects'][i] != None ]
			miss = len(data['objects']) - len(existing_objects)

			# Run through every labeled frame to extract cropped images and to save corresponding label into file
			for i in xrange(len(data['frames'])):
				# print 'this is ', i	
				# print data['frames'][i].keys()
				if len(data['frames'][i].keys()) != 0 and frameCounter[i] == 0:
					for k in xrange(np.max([i - 10, 0]), np.min([i+11, len(data['frames']) ])):
						filename_stem = str(scene_paths[kk]) + '/' + ('%.5d' % k)
						image_file_name = ('%.5d' % k) + '-color.png'
						image_file_name_depth = ('%.5d' % k) + '-depth.png'
						image_path = os.path.join(scene, image_file_name)
						image_path_depth = os.path.join(scene, image_file_name_depth)
						if not (frameCounter[k] == 0 and os.path.exists(image_path)):
							continue
						else:
							frameCounter[k] += 1
							total_count += 1
						# Run through every labeled object in this scene
						objects = []
						for j in xrange(len(data['frames'][i]['polygon'])):
							obj_index = data['frames'][i]['polygon'][j]['object']
							obj_number = object_name_to_order[existing_objects[obj_index - miss]]

							# Get bounding box of objects
							left_x = min(data['frames'][i]['polygon'][j]['x'])
							right_x = max(data['frames'][i]['polygon'][j]['x'])
							left_y = min(data['frames'][i]['polygon'][j]['y'])
							right_y = max(data['frames'][i]['polygon'][j]['y'])

							if right_x <= left_x or right_y <= left_y:
								continue
							substruct = {'class': str(obj_number + 1), 
										'bbox': [left_x, left_y, right_x, right_y],
										'instanceId': 1,
										'difficult': 0,
										'truncated': 0
										}
							objects.append(substruct)
						current_struct = {'objects': objects,
										'type': 'scene',
										'imgsize': [640, 480],
										'frame_no': '%.5d' % total_count,
										'fname': filename_stem}
						if type(current_struct['objects']) != type([]):
							raise
						# gt_data[str(total_count)] = current_struct
						gt_data_list.append(current_struct)
	return gt_data_list

def ground_truth_oneScene_sam(scene_root_path, scene_paths, object_list_file, anotation_path, count_init = 0):
	count_init -= 1
	# Get the map from object class name to a specific number (in alphebatical order)
	object_name_to_order = {}
	with open(object_list_file, 'r') as object_raw_list:
		object_list = object_raw_list.readlines()
		for obj_combo in enumerate(object_list):
			object_name_to_order[obj_combo[1].rstrip()] = obj_combo[0]

	gt_data_list = []
	output_image_number = 0
	total_count = count_init
	for raw_scene in scene_paths:
		scene = os.path.join(scene_root_path, raw_scene)
		anotation_scenepath = os.path.join(anotation_path, raw_scene)
		anotation_filelist = get_immediate_files(anotation_scenepath)

		# Run through every labeled frame to extract cropped images and to save corresponding label into file
		for k in xrange(len(anotation_filelist)):
			filename_stem = raw_scene + '/' + ('%.5d' % k)
			image_file_name = ('%.5d' % k) + '-color.png'
			image_file_name_depth = ('%.5d' % k) + '-depth.png'
			image_path = os.path.join(scene, image_file_name)
			image_path_depth = os.path.join(scene, image_file_name_depth)
			total_count += 1

			# Run through every labeled object in this scene
			objects = []
			anotation_filename = os.path.join(anotation_scenepath, ('%.5d' % k) + '.txt')
			if not os.path.exists(anotation_filename):
				continue
			with open(anotation_filename, 'r') as anotation_file:
				anotation_linelist = anotation_file.readlines()
				if len(anotation_linelist) == 0:
					continue
				for anotation_line_raw in anotation_linelist:
					anotation_line = anotation_line_raw.rstrip().split(' ')
					obj_number = int(anotation_line[0])
					bbox = map(lambda x: float(x), anotation_line[1:])
					# Get bounding box of objects
					substruct = {'class': str(obj_number + 1), 
								'bbox': bbox,
								'instanceId': 1,
								'difficult': 0,
								'truncated': 0
								}
					objects.append(substruct)
				current_struct = {'objects': objects,
								'type': 'scene',
								'imgsize': [640, 480],
								'frame_no': '%.5d' % total_count,
								'fname': filename_stem}
				if type(current_struct['objects']) != type([]):
					raise
				# gt_data[str(total_count)] = current_struct
				gt_data_list.append(current_struct)
	return gt_data_list


def get_immediate_files(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isfile(os.path.join(a_dir, name))]

def get_image_info(image_set, cache=False):
  # ===================================================
  # Modify these lines
	object_list_file = '/home/tom/rgbd-dataset/uwLabels.txt'
	dataset_dir = '/home/tom/object_slam_datasets/rcnn_hha_dataset_loo'
	scene_root_path = '/home/tom/rgbd-scenes-v2/imgs/'
	anotation_path = '/home/tom/object_slam_datasets/rgbd_scene_sam/labels'
  # ===================================================

  	assert os.path.exists(object_list_file)
  	assert os.path.exists(dataset_dir)
  	assert os.path.exists(scene_root_path)
  	cache_filename = '/home/tom/cross-dist/fast-rcnn/' + image_set + '_cache.pkl'

  	if os.path.exists(cache_filename) and cache and False:
  		with open(cache_filename, 'r') as cache_file:
  			return cPickle.load(cache_file)

	number_train_to = 3
	all_scene_paths = ['scene_' + ('%.2d' % k) for k in range(1, 15)]
	train_scene_paths = ['scene_' + ('%.2d' % k) for k in range(1, number_train_to+1)]
	test_scene_paths = ['scene_' + ('%.2d' % k) for k in range(number_train_to+1, 15)]
	# test_scene_paths = ['scene_' + ('%.2d' % k) for k in range(14, 15)]
	# test_scene_paths = ['scene_' + ('%.2d' % k) for k in [10]]
	if image_set == 'train':
		list_1, ind = ground_truth_UW(object_list_file, os.path.join(dataset_dir, 'train' + '_rgb.txt'), object_list_file)
		list_2 = ground_truth_oneScene_sam(scene_root_path, train_scene_paths, object_list_file, anotation_path, ind)
		list_1.extend(list_2)

		# list_1 = ground_truth_oneScene_sam(scene_root_path, train_scene_paths, object_list_file, anotation_path)

	elif image_set == 'test':
		# list_1, ind = ground_truth_UW(object_list_file, os.path.join(dataset_dir, 'test' + '_rgb.txt'), object_list_file)
		# list_2 = ground_truth_oneScene_sam(scene_root_path, test_scene_paths, object_list_file, anotation_path, ind)
		# list_1.extend(list_2)
		list_1 = ground_truth_oneScene_sam(scene_root_path, test_scene_paths, object_list_file, anotation_path)
	elif image_set == 'trainval':
		list_1, ind = ground_truth_UW(object_list_file, os.path.join(dataset_dir, 'train' + '_rgb.txt'), object_list_file)
		list_2, ind2 = ground_truth_UW(object_list_file, os.path.join(dataset_dir, 'test' + '_rgb.txt'), object_list_file)
		list_3, ind3 = ground_truth_oneScene_sam(scene_root_path, all_scene_paths, object_list_file, anotation_path, ind + ind2)
		list_1.extend(list_2)
		list_1.extend(list_3)
	else:
		raise

	with open(cache_filename, 'w') as cache_file:
		cPickle.dump(list_1, cache_file)
	return list_1