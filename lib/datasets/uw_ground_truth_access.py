import os
import re
import cPickle
import itertools
from utils.data_utils import *
import datasets.uw_ground_truth_cache as gt_cache
from uw_config import data_dir_dict, class_sel_list, object_list_filepath

def ground_truth_from_uw_txt(cache_dir, set_fname, cache_type, class_sel_list, step=20):
 	'''
 	Use the training/test set txt file to select which images to use

 	Assuming the structure of the dataset
 	
 	'''

 	assert set_fname[-3:] == 'txt'
 	with open(set_fname, 'r') as set_file:
		content = map(lambda x: x.rstrip().split(' ')[0], set_file.readlines())
		content = map(lambda y: (y[0], os.path.join(cache_dir, y[0], y[1][:-3] + cache_type)), 
						map(lambda x: x.split('/')[-2:], content)
			)

		# Take only 1/step fraction of the original set
		content = content[::step]

		# filter classes
		content = [item[1] for item in content if item[0] in class_sel_list]

	return content

def ground_truth_from_uw_sam(sam_dir, scene_sam_paths, class_list_filename):
 	'''
 	Use the training/test set txt file to select which images to use

 	Assuming the structure of the dataset
 	
 	'''
 	ground_truth_info_list = []

	# Get the map from object class name to a specific class number (in alphebatical order)
	class_name_to_order = {}
	class_order_to_name = {}
	with open(class_list_filename, 'r') as class_raw_list:
		class_list = class_raw_list.readlines()
		for class_combo in enumerate(class_list):
			class_name_to_order[class_combo[1].rstrip()] = class_combo[0]
			class_order_to_name[class_combo[0]] = class_combo[1].rstrip()

 	for proto_sam_path in scene_sam_paths:
 		sam_path = os.path.join(sam_dir, proto_sam_path)
		anotation_filelist = get_immediate_files(sam_path)

		# Run through every labeled frame to extract cropped images and to save corresponding label into file
		for k, fname in enumerate(anotation_filelist):
			filename_stem = proto_sam_path + '/' + fname[:-4]

			# Run through every labeled object in this scene
			object_info_list = []
			with open(os.path.join(sam_path, fname), 'r') as anotation_file:
				anotation_linelist = anotation_file.readlines()
				if len(anotation_linelist) == 0:
					continue
				for anotation_line_raw in anotation_linelist:
					anotation_line = anotation_line_raw.rstrip().split(' ')
					obj_number = int(anotation_line[0])
					bbox = map(lambda x: float(x), anotation_line[1:])

					obj_name = class_order_to_name[obj_number]
					if class_sel_list != None:
						if obj_name not in class_sel_list:
							# obj_name = '__background__'
							continue
					object_info = {'class': obj_name, 
								'bbox': bbox,
								'instanceId': 1,
								'difficult': 0,
								'truncated': 0
								}
					object_info_list.append(object_info)
				if len(object_info_list) == 0:
					continue
				ground_truth_info = {'objects': object_info_list,
								'type': 'scene',
								'imgsize': [640, 480],
								'fname': filename_stem}
				ground_truth_info_list.append(ground_truth_info)

	return ground_truth_info_list

def ground_truth_from_folder(cache_folder_path):
	return get_immediate_files(cache_folder_path)

def ground_truth_from_folders(cache_folder_path_list):
	filename_lists = [get_immediate_files(folder, True) for folder in cache_folder_path_list]
	return list(itertools.chain.from_iterable(filename_lists))

def get_ground_truth_info(image_set, class_sel_list, new_data=False, sam=False, cache=False):
  # ===================================================
  # dirs
	dataset_dir = data_dir_dict.final_dataset_dir
	scene_root_path = data_dir_dict.scene_original_dataset_dir
	cache_dir = data_dir_dict.ground_truth_cache_dir
	sam_dir = data_dir_dict.sam_dir
  # ===================================================

  	if new_data or not os.path.exists(cache_dir):
  		gt_cache.cache(class_sel_list)

  	assert os.path.exists(dataset_dir)
  	assert os.path.exists(scene_root_path)

  	if cache and os.path.exists(cache_filename):
  		assert cache_file_dir is not None and os.path.exists(cache_file_dir)
  		with open(cache_filename, 'r') as cache_file:
  			return cPickle.load(cache_file)

	number_train_to = 3
	all_scene_paths = [os.path.join(cache_dir, 'scene_' + ('%.2d' % k)) for k in range(1, 15)]
	train_scene_paths = [os.path.join(cache_dir, 'scene_' + ('%.2d' % k)) for k in range(1, number_train_to+1)]
	test_scene_paths = [os.path.join(cache_dir, 'scene_' + ('%.2d' % k)) for k in range(number_train_to+1, 15)]

	all_scene_sam_paths = ['scene_' + ('%.2d' % k) for k in range(1, 15)]
	train_scene_sam_paths = ['scene_' + ('%.2d' % k) for k in range(1, number_train_to+1)]
	test_scene_sam_paths = ['scene_' + ('%.2d' % k) for k in range(number_train_to+1, 15)]

	if not sam:
		if image_set == 'train':
			ground_truth_cache_list = ground_truth_from_uw_txt(cache_dir, os.path.join(dataset_dir, 'train_rgb.txt'), 'pkl', class_sel_list)
			train_scene_ground_truth_cache_list = ground_truth_from_folders(train_scene_paths)
			ground_truth_cache_list.extend(train_scene_ground_truth_cache_list)
		elif image_set == 'test':
			ground_truth_cache_list = ground_truth_from_folders(test_scene_paths)
		elif re.match("^sorted_scene_[0-9]+$", image_set):
			_, __, sel_no = image_set.split('_')
			sel_no = int(sel_no)
			if sel_no <= 0 or sel_no > 14:
				raise "start scene no or end scene no of scene dataset out of range!"
			sel_scene_set_path =  os.path.join(cache_dir, 'scene_' + ('%.2d' % sel_no))
			ground_truth_cache_list = ground_truth_from_folders([sel_scene_set_path])
			ground_truth_cache_list.sort()
		elif image_set == 'trainval':
			raise NotImplementedError
		else:
			print image_set
			raise NotImplementedError

		ground_truth_list = []
		for gt_fname in ground_truth_cache_list:
			with open(gt_fname, 'r') as gt_file:
				ground_truth_list.append(cPickle.load(gt_file))
	else:
		ground_truth_list = []
		if image_set == 'train':
			ground_truth_cache_list = ground_truth_from_uw_txt(cache_dir, os.path.join(dataset_dir, 'train_rgb.txt'), 'pkl', class_sel_list)
			for gt_fname in ground_truth_cache_list:
				with open(gt_fname, 'r') as gt_file:
					ground_truth_list.append(cPickle.load(gt_file))
			ground_truth_list.extend(ground_truth_from_uw_sam(sam_dir, train_scene_sam_paths, object_list_filepath))
		elif image_set == 'test':
			ground_truth_list.extend(ground_truth_from_uw_sam(sam_dir, test_scene_sam_paths, object_list_filepath))
		elif image_set == 'trainval':
			raise NotImplementedError
		else:
			raise NotImplementedError





	# if cache:
	# 	with open(lump_cache_fname, 'w') as lump_cache_file:
	# 		cPickle.dump(ground_truth_list, lump_cache_file)

	return ground_truth_list