# ---------------------------------------------------------
# Copyright (c) 2015, Saurabh Gupta
# 
# Licensed under The MIT License [see LICENSE for details]
# ---------------------------------------------------------

import python_utils.evaluate_detection as eval
import python_utils.general_utils as g_utils
import datasets
import datasets.custom_dataset
import re
import os
import datasets.imdb
import xml.dom.minidom as minidom
import numpy as np
import scipy.sparse
import scipy.io as sio
import utils.cython_bbox
import cPickle
import subprocess
from IPython.core.debugger import Tracer
from datasets.custom_ground_truth_access import get_ground_truth_info as get_ground_truth_info
from uw_config import class_sel_list, data_dir_dict

class custom_dataset(datasets.imdb):
    def __init__(self, image_set, image_list, image_type = 'rgb'):
        imdb.__init__(self, 'custom_' + image_type + '_' + image_set)
        self._classes = class_sel_list
        self._class_to_ind = dict(zip(self.classes, xrange(self.num_classes)))
        self._image_type = image_type;
        self._image_set = image_set;
        self._image_ext = '.png'

        self._test_method = test_method
        self._image_list = image_list

        self._image_data = self._load_image_set_data()
        self._image_index, self._index_to_number = self._load_image_set_index()
        # Default to roidb handler
        self._roidb_handler = self.edgeboxes_roidb

    def image_path_at(self, i):
        """
        Return the absolute path to image i in the image sequence.
        """
        return self.image_path_from_index(self._image_index[i])

    def image_path_from_index(self, index):
        """
        Construct an image path from the image's "index" identifier.
        """
        image_path = []
        image_type_list = self._image_type.split('+')
        for typ in image_type_list:
            image_path.append(os.path.join(self._data_path, typ, index + self._image_ext))
            assert os.path.exists(image_path[-1]), 'Path does not exist: {}'.format(image_path)
        return image_path

    def _load_image_set_index(self):
        """
        Load the indexes listed in this dataset's image set file.
        """
        # Example path to image set file:
        # self._devkit_path + /VOCdevkit2007/VOC2007/ImageSets/Main/val.txt
        # image_set_file = os.path.join(self._devkit_path, 'benchmarkData', 'metadata',
        #                               'nyusplits.mat')

        image_index_list = []
        index_to_number = {}
        for ind, item in enumerate(self._image_data):
          image_index_list.append(item['fname'])
          index_to_number[item['fname']] = ind
        
        return image_index_list, index_to_number

    def _load_image_set_data(self):
        """
        Load the indexes listed in this dataset's image set file.
        """

        raw_data = []
        for img_filepath in self._image_list:
          label_filepath = re.sub("JPEGImages", "labels", img_filepath)
          label_filepath = re.sub(".png", ".txt", label_filepath)
          with open(label_filepath, 'r') as label_file:
            labels = map(lambda x: x.rstrip().split(' '), label_file.readlines())

          object_info_list = [
                  {'class': self._classes[label[0]], 
                    'bbox': label[1:],
                    'instanceId': 1,
                    'difficult': 0,
                    'truncated': 0
                    }
                for label in labels]
          if len(object_info_list) == 0:
            continue
          ground_truth_info = {'objects': object_info_list,
                'type': 'scene',
                'imgsize': [640, 480],
                'fname': label_filepath}
          raw_data.append(ground_truth_info)
        return raw_data
    def _get_default_path(self):
        """
        Return the default path where PASCAL VOC is expected to be installed.
        """
        raise NotImplementedError
        return os.path.join('data', 'nyud2')

    def gt_roidb(self):
        """
        Return the database of ground-truth regions of interest.

        This function loads/saves from/to a cache file to speed up future calls.
        """
        # cache_file = os.path.join(self.cache_path, self.name + '_gt_roidb.pkl')
        # if os.path.exists(cache_file) and False:
        #     with open(cache_file, 'rb') as fid:
        #         roidb = cPickle.load(fid)
        #     print '{} gt roidb loaded from {}'.format(self.name, cache_file)
        #     return roidb

        gt_roidb = [self._load_custom_annotation(index)
                    for index in self._image_index]
        with open(cache_file, 'wb') as fid:
            cPickle.dump(gt_roidb, fid, cPickle.HIGHEST_PROTOCOL)
        print 'wrote gt roidb to {}'.format(cache_file)

        return gt_roidb

    def edgeboxes_roidb(self):
        """
        Return the database of selective search regions of interest.
        Ground-truth ROIs are also included.

        This function loads/saves from/to a cache file to speed up future calls.
        """
        cache_file = os.path.join(self.cache_path,
                                  self.name + '_edgeboxes_roidb.pkl')

        if os.path.exists(cache_file) and False:
            with open(cache_file, 'rb') as fid:
                roidb = cPickle.load(fid)
            print '{} ss roidb loaded from {}'.format(self.name, cache_file)
            return roidb

        test_method = 'proposal-only'

        roidb = self.gt_roidb()
        with open(cache_file, 'wb') as fid:
            cPickle.dump(roidb, fid, cPickle.HIGHEST_PROTOCOL)
        print 'wrote gt roidb to {}'.format(cache_file)

        return roidb

    # def _load_edgeboxes_roidb(self, gt_roidb):
    #   # Edgeboxes Proposal version
    #     box_list = []
    #     edgeboxes_path = data_dir_dict.edgeboxes_dataset_dir
    #     for i, image_fname in enumerate(self._image_index):
    #       if self._image_data[i]['type'] == 'category':
    #         box_list.append(np.zeros((2,4)))
    #       else:
    #         edgeboxes_filepath = os.path.join(edgeboxes_path, image_fname + '.txt')
    #         with open(edgeboxes_filepath, 'r') as edgeboxes_file:
    #           box_linelist = edgeboxes_file.readlines()
    #           box_array = np.zeros((len(box_linelist), 4))
    #           for ind, box_line in enumerate(box_linelist):
    #             box_array[ind,:] = np.array(box_line.rstrip().split(' ')).astype('float')
    #           box_list.append(box_array)

    # #     boxes = sio.loadmat(filename)['bboxes'].ravel()
    # #     imnames = sio.loadmat(filename)['imnames'].ravel()
    # #     imnames = [str(x[0]) for x in imnames]
        
    # #     box_list = []
    # #     for i in xrange(len(self._image_index)):
    # #         ind = np.where(self._image_index[i] == np.array(imnames))[0]
    # #         assert(len(ind) == 1)
    # #         box_list.append(boxes[ind[0]][:, (1, 0, 3, 2)] - 1)

        # return self.create_roidb_from_box_list(box_list, gt_roidb)

    def _load_custom_annotation(self, index):
        """
        Load image and bounding boxes info from XML file in the PASCAL VOC
        format.
        """

        # filename = os.path.join(self._devkit_path, 'benchmarkData', \
        #     'gt_box_cache_dir', index + '.mat')
        # print 'Loading: {}'.format(filename)
        raw_data = self._image_data[self._index_to_number[index]]
        objs = raw_data['objects']
        # Select object we care about
        objs = [obj for obj in objs if self._class_to_ind.get(str(obj['class'])) is not None]
        
        num_objs = len(objs)

        boxes = np.zeros((num_objs, 4), dtype=np.uint16)
        gt_classes = np.zeros((num_objs), dtype=np.int32)
        overlaps = np.zeros((num_objs, self.num_classes), dtype=np.float32)

        # Load object bounding boxes into a data frame.
        for ix, obj in enumerate(objs):
            # Make pixel indexes 0-based
            cls = self._class_to_ind.get(str(obj['class']))
            boxes[ix, :] = np.array(obj['bbox']) - 1
            gt_classes[ix] = cls
            overlaps[ix, cls] = 1.0

        overlaps = scipy.sparse.csr_matrix(overlaps)

        return {'boxes' : boxes,
                'gt_classes': gt_classes,
                'gt_overlaps' : overlaps,
                'flipped' : False}
    
    def evaluate_detections(self, all_boxes, output_dir, det_salt = '', eval_salt = '', overlap_thresh = 0.5):
      num_classes = self.num_classes
      num_images = self.num_images
      gt_roidb = self.gt_roidb()
      ap = [[]]; prec = [[]]; rec = [[]]
      ap_file = os.path.join(output_dir, 'eval' + det_salt + eval_salt + '.txt')
      with open(ap_file, 'wt') as f:
          for i in xrange(1, self.num_classes):
              dt = []; gt = [];
              # Prepare the output
              for j in xrange(0,num_images):
                  bs = all_boxes[i][j]
                  if len(bs) == 0:
                    bb = np.zeros((0,4)).astype(np.float32)
                    sc = np.zeros((0,1)).astype(np.float32)
                  else:
                    bb = bs[:,:4].reshape(bs.shape[0],4)
                    sc = bs[:,4].reshape(bs.shape[0],1)
                  dtI = dict({'sc': sc, 'boxInfo': bb})
                  dt.append(dtI)
          
              # Prepare the annotations
              for j in xrange(0,num_images):
                  cls_ind = np.where(gt_roidb[j]['gt_classes'] == i)[0]
                  bb = gt_roidb[j]['boxes'][cls_ind,:]
                  diff = np.zeros((len(cls_ind),1)).astype(np.bool)
                  gt.append(dict({'diff': diff, 'boxInfo': bb}))
              bOpts = dict({'minoverlap': overlap_thresh})
              ap_i, rec_i, prec_i = eval.inst_bench(dt, gt, bOpts)[:3]
              ap.append(ap_i[0]); prec.append(prec_i); rec.append(rec_i)
              ap_str = '{:20s}: ap   {:10f}'.format(self.classes[i], ap_i[0]*100)
              f.write(ap_str + '\n')
              print ap_str
          ap_str = '{:20s}:{:10f}'.format('mean', np.mean(ap[1:])*100)
          # subset = ap[1:]
          # subset = subset[np.logical_not(np.isnan(subset))]
          # ap_str = '{:20s}:{:10f}'.format('mean', np.mean(subset)*100)
          # f.write(ap_str + '\n')
          # print ap_str

      eval_file = os.path.join(output_dir, 'eval' + det_salt + eval_salt + '.pkl')
      g_utils.save_variables(eval_file, [ap, prec, rec, self._classes, self._class_to_ind], \
          ['ap', 'prec', 'rec', 'classes', 'class_to_ind'], overwrite = True)
      eval_file = os.path.join(output_dir, 'eval' + det_salt + eval_salt + '.mat')
      g_utils.scio.savemat(eval_file, {'ap': ap, 'prec': prec, 'rec': rec, 'classes': self._classes}, do_compression = True);
      
      return ap, prec, rec, self._classes, self._class_to_ind
 

if __name__ == '__main__':
    d = datasets.custom_dataset('trainval')
    # res = d.roidb
    from IPython import embed; embed()
