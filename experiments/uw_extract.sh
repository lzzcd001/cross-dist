#!/bin/bash
# Usage:
# ./experiments/scripts/faster_rcnn_end2end.sh GPU NET DATASET [options args to {train,test}_net.py]
# DATASET is either pascal_voc or coco.
#
# Example:
# ./experiments/scripts/faster_rcnn_end2end.sh 0 VGG_CNN_M_1024 pascal_voc \
#   --set EXP_DIR foobar RNG_SEED 42 TRAIN.SCALES "[400, 500, 600, 700]"

set -x
set -e
tr_set='train'
test_set='test'

THRESH=$1
SAVEPATH=$2

model='alexnet_rgb_alexnet_hha';
PYTHONPATH='.' PYTHONUNBUFFERED="True"
modality="rgb+hha";
time ./tools/uw_scene_extract_pred.py --gpu 0 \
  --def output/training_demo/$model/test.prototxt.$modality \
  --net output/training_demo/$model/uw_rgb+hha_$tr_set/fast_rcnn_iter_40000.caffemodel \
  --cfg output/training_demo/$model/config.prototxt."$modality" \
  --thresh ${THRESH} \
  --save_path ${SAVEPATH} \