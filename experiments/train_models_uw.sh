# # Train the HHA Alexnet model from the supervision transfer initialization weights
# PYTHONPATH='.' PYTHONUNBUFFERED="True" python tools/train_net.py --gpu 0 \
#   --solver output/training_demo/alexnet_hha/solver.prototxt.hha \
#   --weights data/init_models/ST_vgg_to_alexnet_hha/ST_vgg_to_alexnet_hha.caffemodel \
#   --imdb uw_hha_train \
#   --cfg output/training_demo/alexnet_hha/config.prototxt.hha \
#   --iters 40000 \
#   2>&1 | tee output/training_demo/alexnet_hha/train.log.hha

# # # # Train the VGG model on the color rgb
# # # PYTHONPATH='.' PYTHONUNBUFFERED="True" python tools/train_net.py --gpu 1 \
# # #   --solver output/training_demo/vgg_rgb/solver.prototxt.rgb \
# # #   --weights data/init_models/VGG16/VGG16.v2.caffemodel \
# # #   --imdb uw_rgb_train \
# # #   --cfg output/training_demo/vgg_rgb/config.prototxt.rgb \
# # #   2>&1 | tee output/training_demo/vgg_rgb/train.log.rgb

# # Train the alexnet model on the color rgb
# PYTHONPATH='.' PYTHONUNBUFFERED="True" python tools/train_net.py --gpu 0 \
#   --solver output/training_demo/alexnet_rgb/solver.prototxt.rgb \
#   --weights data/init_models/CaffeNet/CaffeNet.v2.caffemodel \
#   --imdb uw_rgb_train \
#   --cfg output/training_demo/alexnet_rgb/config.prototxt.rgb \
#   2>&1 | tee output/training_demo/alexnet_rgb/train.log.rgb

# # Merge alexnet HHA and alexnet RGB models using by net surgery
# mkdir output/training_demo/alexnet_rgb_alexnet_hha/uw_rgb+hha_train
# PYTHONPATH='.' python python_utils/do_net_surgery.py \
#   --out_net_def output/training_demo/alexnet_rgb_alexnet_hha/test.prototxt.rgb+hha \
#   --net_surgery_json output/training_demo/alexnet_rgb_alexnet_hha/init_weights.json \
#   --out_net_file output/training_demo/alexnet_rgb_alexnet_hha/uw_rgb+hha_train/fast_rcnn_iter_40000.caffemodel

# # Merge alexnet HHA and VGG RGB models using by net surgery
# mkdir output/training_demo/vgg_rgb_alexnet_hha/uw_rgb+hha_train
# PYTHONPATH='.' python python_utils/do_net_surgery.py \
#   --out_net_def output/training_demo/vgg_rgb_alexnet_hha/test.prototxt.rgb+hha \
#   --net_surgery_json output/training_demo/vgg_rgb_alexnet_hha/init_weights.json \
#   --out_net_file output/training_demo/vgg_rgb_alexnet_hha/uw_rgb+hha_train/fast_rcnn_iter_40000.caffemodel

# Testing alexnet_rgb and alexnet_hha models
model='alexnet_rgb_alexnet_hha'; tr_set='train'; test_set='test'; modality="rgb+hha";
PYTHONPATH='.' PYTHONUNBUFFERED="True" python tools/test_net.py --gpu 0 \
  --def output/training_demo/$model/test.prototxt.$modality \
  --net output/training_demo/$model/uw_rgb+hha_$tr_set/fast_rcnn_iter_40000.caffemodel \
  --imdb uw_"$modality"_"$test_set" \
  --cfg output/training_demo/$model/config.prototxt."$modality"

# # Testing vgg_rgb and alexnet_hha models
# model='vgg_rgb_alexnet_hha'; tr_set='train'; test_set='test'; modality="rgb+hha";
# PYTHONPATH='.' PYTHONUNBUFFERED="True" python tools/test_net.py --gpu 0 \
#   --def output/training_demo/$model/test.prototxt.$modality \
#   --net output/training_demo/$model/uw_rgb+hha_$tr_set/fast_rcnn_iter_40000.caffemodel \
#   --imdb uw_"$modality"_"$test_set" \
#   --cfg output/training_demo/$model/config.prototxt."$modality"

# # Testing alexnet_hha models
# model='alexnet_hha'; tr_set='train'; test_set='test'; modality="hha";
# PYTHONPATH='.' PYTHONUNBUFFERED="True" python tools/test_net.py --gpu 0 \
#   --def output/training_demo/$model/test.prototxt.$modality \
#   --net output/training_demo/$model/uw_"$modality"_$tr_set/fast_rcnn_iter_40000.caffemodel \
#   --imdb uw_"$modality"_"$test_set" \
#   --cfg output/training_demo/$model/config.prototxt."$modality"

# # Testing alexnet_rgb models
# model='alexnet_rgb'; tr_set='train'; test_set='test'; modality="rgb";
# PYTHONPATH='.' PYTHONUNBUFFERED="True" python tools/test_net.py --gpu 0 \
#   --def output/training_demo/$model/test.prototxt.$modality \
#   --net output/training_demo/$model/uw_"$modality"_$tr_set/fast_rcnn_iter_40000.caffemodel \
#   --imdb uw_"$modality"_"$test_set" \
#   --cfg output/training_demo/$model/config.prototxt."$modality"

# # Testing vgg_rgb models
# model='vgg_rgb'; tr_set='train'; test_set='test'; modality="rgb";
# PYTHONPATH='.' PYTHONUNBUFFERED="True" python tools/test_net.py --gpu 0 \
#   --def output/training_demo/$model/test.prototxt.$modality \
#   --net output/training_demo/$model/uw_"$modality"_$tr_set/fast_rcnn_iter_40000.caffemodel \
#   --imdb uw_"$modality"_"$test_set" \
#   --cfg output/training_demo/$model/config.prototxt."$modality"
