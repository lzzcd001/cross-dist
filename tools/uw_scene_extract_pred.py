#!/usr/bin/env python

# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

"""Test a Fast R-CNN network on an image database."""

import _init_paths
from fast_rcnn.test import scene_pred
from fast_rcnn.config import cfg, cfg_from_file
import caffe
import argparse
import pprint
import time, os, sys
from datasets.uw_config import data_dir_dict
from datasets.uw_dataset import uw_dataset
from utils.data_utils import exist_or_mkdir

def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(description='Test a Fast R-CNN network')
    parser.add_argument('--gpu', dest='gpu_id', help='GPU id to use',
                        default=0, type=int)
    parser.add_argument('--def', dest='prototxt',
                        help='prototxt file defining the network',
                        default=None, type=str)
    parser.add_argument('--net', dest='caffemodel',
                        help='model to test',
                        default=None, type=str)
    parser.add_argument('--cfg', dest='cfg_file',
                        help='optional config file', default=None, type=str)
    parser.add_argument('--wait', dest='wait',
                        help='wait until net file exists',
                        default=True, type=bool)
    parser.add_argument('--imdb', dest='imdb_name',
                        help='dataset to test',
                        default='voc_2007_test', type=str)
    parser.add_argument('--comp', dest='comp_mode', help='competition mode',
                        action='store_true')
    parser.add_argument('--thresh', dest='thresh', help='threshold of minimum probability',
                        default=0.4, type=float)
    parser.add_argument('--save_path', dest='save_path', help='threshold of minimum probability',
                        type=str)

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = parse_args()

    print('Called with args:')
    print(args)

    if args.cfg_file is not None:
        cfg_from_file(args.cfg_file)

    print('Using config:')
    pprint.pprint(cfg)

    while not os.path.exists(args.caffemodel) and args.wait:
        print('Waiting for {} to exist...'.format(args.caffemodel))
        time.sleep(10)

    caffe.set_mode_gpu()
    caffe.set_device(args.gpu_id)
    net = caffe.Net(args.prototxt, args.caffemodel, caffe.TEST)
    net.name = os.path.splitext(os.path.basename(args.caffemodel))[0]

    exist_or_mkdir(args.save_path)
    for iter_no in xrange(1,15):
        imdb = uw_dataset('sorted_scene_%d' % iter_no, image_type = 'rgb+hha', devkit_path=data_dir_dict.final_dataset_dir)
        detection_strs = scene_pred(net, imdb, thresh=args.thresh)
        with open(os.path.join(args.save_path, 'scene_%02d.txt' % iter_no), 'w') as save_file:
            save_file.writelines(detection_strs)
